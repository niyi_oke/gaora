/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.ejb;

import com.silverbullet.gaora.entities.MailConfig;
import com.silverbullet.gaora.util.MessageLogger;
import java.io.File;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class EmailManager {

    @PersistenceContext
    EntityManager em;

    @Asynchronous
    public void sendPlainEmail(String sender, String subject, String message, String ...to) {
        List<MailConfig> mailConfigs
            = em.createNamedQuery("MailConfig.fetchActive", MailConfig.class).getResultList();
        if (mailConfigs.isEmpty()) {

        } else {
            MailConfig mailConfig = mailConfigs.get(0);
            Email email = new SimpleEmail();
            email.setHostName(mailConfig.getHost());
            email.setSmtpPort(mailConfig.getPort());
            email.setAuthenticator(new DefaultAuthenticator(mailConfig.
                getUserName(), mailConfig.getPassword()));
            email.setSSLOnConnect(true);
            try {
                email.setFrom(sender);
                email.setSubject(subject);
                email.setMsg(message);
                email.addTo(to);
                email.send();
            } catch (EmailException ex) {
                MessageLogger.debug(EmailManager.class, ex.getMessage());
            }
        }

    }

    @Asynchronous
    public void sendHtmlEmail(String sender, String subject, String htmlBody, String textAlt, String... to) {
        List<MailConfig> mailConfigs
            = em.createNamedQuery("MailConfig.fetchActive", MailConfig.class).getResultList();
        if (mailConfigs.isEmpty()) {

        } else {
            MailConfig mailConfig = mailConfigs.get(0);
            try {
                HtmlEmail email = new HtmlEmail();
                email.setHostName(mailConfig.getHost());
                email.setSmtpPort(mailConfig.getPort());
                email.setAuthenticator(new DefaultAuthenticator(mailConfig.getUserName(),
                    mailConfig.getPassword()));
                email.setSSLOnConnect(true);
                email.addTo(to);
                email.setFrom(sender);
                email.setSubject(subject);
                // set the html message
                email.setHtmlMsg(htmlBody);
                // set the alternative message
                email.setTextMsg(textAlt);
                // send the email
                email.send();
            } catch (EmailException ex) {
                MessageLogger.debug(EmailManager.class, ex.getMessage());
            }
        }
    }

    @Asynchronous
    public void sendHtmlEmailWithAttachment(String sender, String subject, String htmlBody,
        String textAlt, String to, File... files) {
        List<MailConfig> mailConfigs
            = em.createNamedQuery("MailConfig.fetchActive", MailConfig.class).getResultList();
        if (mailConfigs.isEmpty()) {

        } else {
            MailConfig mailConfig = mailConfigs.get(0);
            try {
                HtmlEmail email = new HtmlEmail();
                email.setHostName(mailConfig.getHost());
                email.setSmtpPort(mailConfig.getPort());
                email.setAuthenticator(new DefaultAuthenticator(mailConfig.getUserName(),
                    mailConfig.getPassword()));
                email.setSSLOnConnect(true);
                email.addTo(to);
                for (File file : files) {
                    email.attach(file);
                }
                email.setFrom(sender);
                email.setSubject(subject);
                email.setHtmlMsg(htmlBody);
                email.setTextMsg(textAlt);

                email.send();
            } catch (EmailException ex) {
                MessageLogger.debug(EmailManager.class, ex.getMessage());
            }
        }
    }

    @Asynchronous
    public void sendHtmlEmailWithAttachment(String sender, String subject, String htmlBody, String textAlt, String to,
        String... files) {
        List<MailConfig> mailConfigs
            = em.createNamedQuery("MailConfig.fetchActive", MailConfig.class).getResultList();
        if (mailConfigs.isEmpty()) {

        } else {
            MailConfig mailConfig = mailConfigs.get(0);
            try {
                HtmlEmail email = new HtmlEmail();
                email.setHostName(mailConfig.getHost());
                email.setSmtpPort(mailConfig.getPort());
                email.setAuthenticator(new DefaultAuthenticator(mailConfig.getUserName(),
                    mailConfig.getPassword()));
                email.setSSLOnConnect(true);
                email.addTo(to);

                for (String file : files) {
                    email.attach(new File(file));
                }
                email.setFrom(sender);
                email.setSubject(subject);
                email.setHtmlMsg(htmlBody);
                email.setTextMsg(textAlt);

                email.send();
            } catch (EmailException ex) {
                MessageLogger.debug(EmailManager.class, ex.getMessage());
            }
        }
    }

   

//    @Asynchronous
//    public void sendHtmlEmail() throws MalformedURLException, EmailException {
//        System.out.println(new Date());
//        HtmlEmail email = new HtmlEmail();
//        email.setHostName("smtp.gmail.com");
//        email.setSmtpPort(465);
//        email.setAuthenticator(new DefaultAuthenticator("prodigy4442", "opeyemi1411Ojo"));
//        email.setSSLOnConnect(true);
//        email.addTo("prodigy4440@yahoo.com", "AZEEZ Isau Opeyemi");
//        email.setFrom("prodigy4442@gmail.com", "Emi ni me");
//        email.setSubject("Test email with inline image");
//
//        // embed the image and get the content id
//        URL url = new URL("http://www.apache.org/images/asf_logo_wide.gif");
//        String cid = email.embed(url, "Apache logo");
//
//        // set the html message
//        email.setHtmlMsg("<html>The apache logo - <img src=\"cid:" + cid + "\"></html>");
//
//        // set the alternative message
//        email.setTextMsg("Your email client does not support HTML messages");
//
//        // send the email
//        email.send();
//
//    }
//    public static void main(String[] args) throws EmailException {
//        String send = "";
//        Email email = new SimpleEmail();
//        email.setHostName("mail.uregista.com");
//        email.setSmtpPort(587);
//        email.setAuthenticator(new DefaultAuthenticator("support@uregista.com", "_!}vsL[t#*I$"));
//        email.setSSLOnConnect(true);
//        try {
//            email.setFrom("support@uregista.com","URegista");
//            email.setSubject("URegista Mail");
//            email.setMsg("This is the message body.");
//            email.addTo("prodigy4440@yahoo.com");
//
//            send = email.send();
//            System.out.println(send);
//            L.info("-------------" + send);
//        } catch (EmailException ex) {
//            L.error("Exception sending mail : " + ex.getMessage());
//        } finally {
//            L.info(send);
//        }
//        new EmailManager().sendPlainEmailGeneralConf("URegista Mail", "This is the message body.", "prodigy4440@yahoo.com");
//    }
//    }
}
