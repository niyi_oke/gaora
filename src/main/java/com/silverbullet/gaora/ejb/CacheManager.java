/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.ejb;

import java.time.Clock;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.cache.Cache;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.AccessedExpiryPolicy;
import javax.cache.expiry.Duration;
import javax.cache.spi.CachingProvider;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

/**
 * A Stateless Bean(JCache Wrapper) for caching data, For instance the login information of the user in a system can be
 * cached where the key is the token and the Value is the data of the user to be cached. Cached data expires every 10-10
 * minutes.
 *
 * @author prodigy4440
 * @param <K> key
 * @param <V> Value
 */
@Singleton
public class CacheManager<K, V> {

  private static final String CACHENAME = "gaora-cache-" + Clock.systemUTC().millis();

  private Cache<K, V> cache;

  private List<K> keys;

  /**
   * Setting up of the cache
   */
  @PostConstruct
  private void init() {
    CachingProvider provider = Caching.getCachingProvider();
    javax.cache.CacheManager cm = provider.getCacheManager();
    MutableConfiguration<K, V> config = new MutableConfiguration<>();
    config.setStoreByValue(true)
            .setExpiryPolicyFactory(AccessedExpiryPolicy.factoryOf(Duration.TEN_MINUTES))
            .setStatisticsEnabled(false);
    cache = cm.createCache(CACHENAME, config);
    cache = cm.getCache(CACHENAME);
    keys = new CopyOnWriteArrayList<>();
  }

  /**
   * Fetch the value of a cached using the supplied key.
   *
   * @param key The key of the data
   * @return The Value of the cached Data of null if it doesn't exist.
   */
  @Lock(LockType.READ)
  public V get(K key) {
    return cache.get(key);
  }

  /**
   * Confirms if a key was used in caching data
   *
   * @param key The key of the data
   * @return true if the data is cache, false otherwise.
   */
  @Lock(LockType.READ)
  public boolean contains(K key) {
    if (Objects.isNull(key)) {
      return false;
    }
    return cache.containsKey(key);
  }

  /**
   * Caches a key, value pair data
   *
   * @param key The key of the data to be cached
   * @param value The value to be cached
   */
  @Lock(LockType.WRITE)
  public void put(K key, V value) {
    cache.put(key, value);
    keys.add(key);
  }

  /**
   * Removes a data that has been cached
   *
   * @param key The key of the data, that we intend to remove
   */
  @Lock(LockType.WRITE)
  public void remove(K key) {
    cache.remove(key);
    keys.remove(key);
  }

  /**
   * Fetches all the data that has been cached
   *
   * @return A Map of all the data that has been cached
   */
  @Lock(LockType.READ)
  public Map<K, V> getAll() {
    return cache.getAll(keys.stream().collect(Collectors.toSet()));
  }

  /**
   * Clear all the data that has been cached
   */
  @Lock(LockType.WRITE)
  public void clear() {
    keys.clear();
    cache.clear();
  }

  /**
   * Cleanup for cache
   */
  @PreDestroy
  private void shutdown() {
    cache.getCacheManager().close();
  }

}
