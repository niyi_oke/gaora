/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Stateless Bean whose instance can be used in producing a ManagedExecutorService and ManagedScheduledExecutorService
 * for concurrency purposes.
 *
 * @author prodigy4440
 */
@Singleton
@Startup
public class ConcurrencyManager {

  @Resource(lookup = "java:comp/DefaultManagedExecutorService")
  private ManagedExecutorService executorService;

  @Resource(lookup = "java:comp/DefaultManagedScheduledExecutorService")
  private ManagedScheduledExecutorService scheduledExecutorService;

  private final Logger L = LoggerFactory.getLogger(ConcurrencyManager.class);

  @PostConstruct
  public void init() {
    L.info("Initializing Concurrency Manager");
    L.info("Managed Executors Service : " + executorService);
    L.info("Scheduled Managed Executors Service : " + scheduledExecutorService);
  }

  public ManagedExecutorService getManagedExecutorService() {
    return this.executorService;
  }

  public ManagedScheduledExecutorService getManagedScheduledExecutorService() {
    return this.scheduledExecutorService;
  }

}
