/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.responses;

/**
 *
 * @author prodigy4440
 */
public class ResponseCodes {

    public static final int REQUEST_SUCCESSFUL = 0;
    public static final int USER_ALREADY_LOGIN = 1;
    public static final int PARTIAL_SUCCESS = 10;

    public static final int INVALID_PHONE_NUMBER = 100;
    public static final int INVALID_EMAIL = 101;
    public static final int BAD_INPUT_PARAM = 102;
    public static final int FORGOT_PASSWORD_REQUEST_EXIST = 105;
    public static final int WRONG_TOKEN = 107;
    public static final int INPUT_ERROR = 109;
    
    public static final int BANK_ACCOUNT_SUSPENDED = 120;
    public static final int PAYMENT_FAILURE = 121;
    public static final int INSUFFICIENT_BALANCE = 125;
    public static final int PAYMENT_URL_FAILURE = 126;
    public static final int PAYMENT_AUTHOCHARGE_FAILURE = 127;
    
    

    public static final int INVALID_ACCOUNT = 200;
    public static final int ACCOUNT_ALREADY_EXISTS = 201;
    public static final int ACCOUNT_DOES_NOT_EXIST = 202;
    public static final int ACCESS_DENIED = 203;
    public static final int PASSWORD_MISMATCH = 204;
    public static final int INVALID_CREDENTIALS = 205;
    public static final int ACCOUNT_DISABLED = 207;
    public static final int EMAIL_ALREADY_EXISTS = 208;
    public static final int ACCOUNT_AUTHENTICATION_ERROR = 210;
    public static final int ACCOUNT_UPDATE_FAILED = 211;
    public static final int USER_NOT_LOGGED_IN = 212;
    public static final int USER_UNAUTHORIZED = 213;

    public static final int OPERATION_NOT_PERMITTED = 220;
    public static final int OPERATION_NOT_SUPPORTED = 221;
    public static final int SESSION_ALREADY_EXISTS = 222;

    public static final int TRANSACTION_ALREADY_EXISTS_SUCCESS = 300;
    public static final int TRANSACTION_ALREADY_EXISTS_FAILED = 301;
    public static final int TRANSACTION_FAILED = 302;
    public static final int LOST_OR_STOLEN_PIN = 306;
    public static final int INSUFFICIENT_FUNDS = 309;
    public static final int NO_RECORD = 310;
    public static final int INVALID_PAYMENT_PARTNER = 311;
    public static final int INVALID_PROVIDER = 312;
    public static final int RECORD_ALREADY_EXISTS = 313;
    public static final int TRANSACTION_RECORD_EXPIRED = 314;
    public static final int RESOURCE_NOT_FOUND = 315;
    public static final int ERROR_READING_RESOURCE = 316;
    public static final int CONSTRAINT_VIOLATION = 317;
    public static final int INAVLID_IMAGE_FORMAT = 318;
    public static final int INVALID_FILE_FORMAT = 319;
    public static final int TRANSACTION_SUCCESSFUL = 320;
    public static final int TRANSACTION_STATUS_UNKNOWN = 321;
    public static final int TRANSACTION_PENDING = 322;
    

    public static final int REQUESTER_AUTH_ERROR = 901;
    public static final int SERVER_AUTH_ERROR = 902;
    public static final int INTERNAL_SYSTEM_ERROR = 903;
    public static final int SERVICE_ERROR = 905;
    public static final int DATABASE_ERROR = 906;
    public static final int FIILESYSTEM_ERROR = 907;

}
