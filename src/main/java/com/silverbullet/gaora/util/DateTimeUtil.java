/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author prodigy4440
 */
public class DateTimeUtil {

    public Pair<Date, Date> getDateRange() {
        Date begining, end;

        {
            Calendar calendar = getCalendarForNow();
            calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            setTimeToBeginningOfDay(calendar);
            begining = calendar.getTime();
        }

        {
            Calendar calendar = getCalendarForNow();
            calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            setTimeToEndofDay(calendar);
            end = calendar.getTime();
        }

        return Pair.createPair(begining, end);
    }

    private static Calendar getCalendarForNow() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(ZonedDateTime zonedDateTime) {
        return Date.from(zonedDateTime.toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date startOfTheMonth() {
        LocalDateTime with = LocalDateTime.now().with(TemporalAdjusters.firstDayOfMonth());
        return asDate(with);
    }

    public static Date endOfLastMonth() {
        LocalDateTime with = LocalDateTime.now().with(TemporalAdjusters.lastDayOfMonth()).minusMonths(1);
        return asDate(with);
    }

    public static Date startOfTheYear() {
        LocalDateTime with = LocalDateTime.now().with(TemporalAdjusters.firstDayOfYear());
        return asDate(with);
    }

    public static Date endOfTheYear() {
        LocalDateTime with = LocalDateTime.now().with(TemporalAdjusters.lastDayOfYear());
        return asDate(with);
    }

    public static Date startOfDay() {
        LocalDateTime of = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
        return asDate(of);
    }

    public static Date endOfDay() {
        LocalDateTime of = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
        return asDate(of.plusHours(23));
    }

    public static Date dateFromString(String sDate) {
        try {
            LocalDate parse = LocalDate.parse(sDate);
            return asDate(parse);
        } catch (DateTimeParseException dtpe) {
            return asDate(LocalDate.now());
        }
    }

    public static Date dateFromString(String sDate, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        try {
            LocalDate parse = LocalDate.parse(sDate, formatter);
            return asDate(parse);
        } catch (DateTimeParseException dtpe) {
            return asDate(LocalDate.now());
        }
    }

    public static Date dateTimeFromString(String sDate, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        try {
            LocalDateTime parse = LocalDateTime.parse(sDate, formatter);
            return asDate(parse);
        } catch (DateTimeParseException dtpe) {
            return asDate(LocalDateTime.now());
        }
    }

    public static Date startOfTheWeek() {
        ZonedDateTime firstOfWeek = ZonedDateTime.now().with(ChronoField.DAY_OF_WEEK, 1);
        return asDate(firstOfWeek);
    }

    public static Date endOfTheWeek() {
        return asDate(ZonedDateTime.now().with(ChronoField.DAY_OF_WEEK, 1).plusWeeks(1).minusDays(1));
    }

    public static Date getBeginingOfLastYear() {
        LocalDateTime with = LocalDateTime.now().with(TemporalAdjusters.firstDayOfYear()).minusYears(1);
        return asDate(with);
    }

    public static long secondsDifferenceFromNow(Date date) {
        return Duration.between(currentDateTime(), asLocalDateTime(date)).getSeconds();
    }

    public static long minutesDifferenceFromNow(Date date) {
        return Duration.between(currentDateTime(), asLocalDateTime(date)).toMinutes();
    }

    public static long hoursDifferenceFromNow(Date date) {
        return Duration.between(currentDateTime(), asLocalDateTime(date)).toHours();
    }

    public static int daysDifferenceFromNow(Date date) {
        return Period.between(asLocalDate(asDate(currentDateTime())), asLocalDate(date)).getDays();
    }

    public static int monthsDifferenceFromNow(Date date) {
        return Period.between(asLocalDate(asDate(currentDateTime())), asLocalDate(date)).getMonths();
    }

    public static int yearsDifferenceFromNow(Date date) {
        return Period.between(asLocalDate(asDate(currentDateTime())), asLocalDate(date)).getYears();
    }

    public static LocalDate currentDate() {
        LocalDate localDate = LocalDate.now(ZoneId.of("GMT+1"));
        return localDate;
    }

    public static LocalDateTime currentDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("GMT+1"));
        return localDateTime;
    }

    public static Date currentDatePlusMonths(int months) {
        return asDate(currentDateTime().plusMonths(months));
    }

    public static Date currentDatePlusYears(int years) {
        return asDate(currentDateTime().plusYears(years));
    }

    public static Date datePlusMonths(Date date, int months) {
        return asDate(asLocalDateTime(date).plusMonths(months));
    }

    public static Date datePlusYears(Date date, int years) {
        return asDate(asLocalDateTime(date).plusYears(years));
    }

    public static Date datePlusHours(Date date, int houurs) {
        return asDate(asLocalDateTime(date).plusHours(houurs));
    }

    public static Date datePlusMinutes(Date date, int minutes) {
        return asDate(asLocalDateTime(date).plusMinutes(minutes));
    }

    public static Date currentDatePlusHours(int hours) {
        return asDate(currentDateTime().plusHours(hours));
    }

    public static Date currentDatePlusMinutes(int minutes) {
        return asDate(currentDateTime().plusMinutes(minutes));
    }

}
