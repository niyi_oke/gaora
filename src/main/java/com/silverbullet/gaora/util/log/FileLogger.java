/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util.log;

import com.silverbullet.gaora.util.DateTimeUtil;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import org.slf4j.LoggerFactory;

/**
 *
 * @author prodigy4440
 */
public class FileLogger extends AbstractLogger {

  private static final org.slf4j.Logger L = LoggerFactory.getLogger(FileLogger.class);

  private static final String INFOFILE = "info";
  private static final String ACTIVITYFILE = "activity";
  private static final String DEBUGFILE = "debug";
  private static final String ERRORFILE = "error";
  private static final String FILE_EXT = ".log";
  private static final String FILE_NAME_SEPARATOR = "-";

  private PrintWriter infoWriter;
  private PrintWriter activityWriter;
  private PrintWriter debugWriter;
  private PrintWriter errorWriter;

  public FileLogger(String dir) {
    try {
      Files.createDirectories(Paths.get(dir));
    } catch (IOException ex) {
      L.error(ex.getMessage());
    }

    try {
      BufferedWriter infoBufferedWriter = Files.newBufferedWriter(Paths.get(dir+"/"+
              INFOFILE + FILE_NAME_SEPARATOR + DateTimeUtil.currentDate() + FILE_EXT), Charset.forName("UTF8"),
              StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
      infoWriter = new PrintWriter(infoBufferedWriter, true);
    } catch (IOException e) {
      L.error("Failed to create PrintWriter: " + e.getMessage());
    }

    try {
      BufferedWriter activityBufferedWriter = Files.newBufferedWriter(Paths.get(dir+"/"+
              ACTIVITYFILE + FILE_NAME_SEPARATOR + DateTimeUtil.currentDate() + FILE_EXT), Charset.forName("UTF8"),
              StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
      activityWriter = new PrintWriter(activityBufferedWriter, true);
    } catch (IOException e) {
      L.error("Failed to create PrintWriter: " + e.getMessage());
    }

    try {
      BufferedWriter debugBufferedWriter = Files.newBufferedWriter(Paths.get(dir+"/"+
              DEBUGFILE + FILE_NAME_SEPARATOR + DateTimeUtil.currentDate() + FILE_EXT), Charset.forName("UTF8"),
              StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
      debugWriter = new PrintWriter(debugBufferedWriter, true);
    } catch (IOException e) {
      L.error("Failed to create PrintWriter: " + e.getMessage());
    }
    
    try {
      BufferedWriter errorBufferedWriter = Files.newBufferedWriter(Paths.get(dir+"/"+
              ERRORFILE + FILE_NAME_SEPARATOR + DateTimeUtil.currentDate() + FILE_EXT), Charset.forName("UTF8"),
              StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
      errorWriter = new PrintWriter(errorBufferedWriter, true);
    } catch (IOException e) {
      L.error("Failed to create PrintWriter: " + e.getMessage());
    }
  }

  @Override
  public void log(String message, Date date, Level level) {
    String logMessage = date + " : " + level + " : " + message;
    switch (level) {
      case ACTIVITY:
        activityWriter.println(logMessage);
        break;
      case DEBUG:
        debugWriter.println(logMessage);
        break;
      case ERROR:
        errorWriter.println(logMessage);
        break;
      case INFO:
        infoWriter.println(logMessage);
        break;
      default:
        infoWriter.println(logMessage);
    }
  }

  public void close() {
    infoWriter.close();
    activityWriter.close();
    debugWriter.close();
    errorWriter.close();
  }

}
