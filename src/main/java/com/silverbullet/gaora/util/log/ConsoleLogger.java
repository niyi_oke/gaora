/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util.log;

import java.util.Date;
import org.slf4j.LoggerFactory;

/**
 *
 * @author prodigy4440
 */
public class ConsoleLogger extends AbstractLogger {

  org.slf4j.Logger L = LoggerFactory.getLogger(ConsoleLogger.class);

  @Override
  public void log(String message, Date date, Level level) {
    if (message == null) {
      message = "";
    }

    if (date == null) {
      date = new Date();
    }

    if (level == null) {
      level = Level.INFO;
    }

    String logMessage = date + " : " + level + " : " + message;
    switch (level) {
      case ACTIVITY:
        L.info(logMessage);
        break;
      case DEBUG:
        L.debug(logMessage);
        break;
      case INFO:
        L.info(logMessage);
        break;
      case ERROR:
        L.error(logMessage);
        break;
      default:
        L.info(logMessage);
    }
  }

}
