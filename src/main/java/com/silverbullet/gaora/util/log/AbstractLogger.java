/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util.log;

import java.util.Date;

/**
 *
 * @author prodigy4440
 */
public abstract class AbstractLogger implements ILogger {

  @Override
  public void log(String message) {
    log(message, new Date());
  }

  @Override
  public void log(String message, Date date) {
    log(message, date, Level.INFO);
  }

  @Override
  public void log(String message, Level level) {
    log(message, new Date(), level);
  }

  @Override
  public abstract void log(String message, Date date, Level level);

}
