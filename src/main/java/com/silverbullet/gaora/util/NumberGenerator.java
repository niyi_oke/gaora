/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util;

import java.util.Random;

/**
 *
 * @author prodigy4440
 */
public class NumberGenerator {
  
  private static final String TRANSACTION_REF = "abcdefghijklmnopqrstuvwxyzAZBDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  private static final String ACCOUNT_NUMBERS = "0123456789";
  private static final Random RANDOM = new Random();

  public static String nextNumber(int length) {
    return next(length, ACCOUNT_NUMBERS);
  }
  
  public static String nextTransactionRef(){
    return next(3, TRANSACTION_REF)+next(8, ACCOUNT_NUMBERS)+next(5, TRANSACTION_REF);
  }
  
  public static String next(int length, String PARAM) {
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      sb.append(PARAM.charAt(RANDOM.nextInt(PARAM.length())));
    }
    return sb.toString();
  }

}
