/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.annotations;

import com.silverbullet.gaora.util.log.ConsoleLogger;
import com.silverbullet.gaora.util.log.DatabaseLogger;
import com.silverbullet.gaora.util.log.FileLogger;
import com.silverbullet.gaora.util.log.Level;
import com.silverbullet.gaora.util.log.Type;
import java.lang.reflect.Method;
import java.util.Arrays;
import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author prodigy4440
 */
@Priority(Interceptor.Priority.APPLICATION + 10)
@Interceptor
@Log
public class LogInterceptor {

    @EJB
    private DatabaseLogger databaseLogger;

    private final ConsoleLogger consoleLogger;

    private final FileLogger fileLogger;

    public LogInterceptor() throws NamingException {
        InitialContext initialContext = new InitialContext();
        String aname = (String) initialContext.lookup("java:app/AppName");
        String filePath = System.getProperty("jboss.home.dir")
            + "/standalone/silverbullet/gaora/" + aname + "/logs";
        consoleLogger = new ConsoleLogger();
        fileLogger = new FileLogger(filePath);
    }

    @AroundInvoke
    public Object log(InvocationContext context) throws Exception {
        String logMessage = "";
        Method method = context.getMethod();
        String methodName = method.getName();
        logMessage = logMessage + methodName;

        String parameters = Arrays.toString(context.getParameters());
        logMessage = logMessage + parameters;

        Object proceed = context.proceed();
        logMessage = logMessage + ", Returned: " + proceed + ".";

        Log log = context.getMethod().getAnnotation(Log.class);
        Level level = log.level();
        Type[] type = log.type();

        for (Type logType : type) {
            switch (logType) {
                case CONSOLE:
                    consoleLogger.log(logMessage, level);
                    break;
                case DATABASE:
                    databaseLogger.log(logMessage, level);
                    break;
                case EMAIL:
                    break;
                case FILE:
                    fileLogger.log(logMessage, level);
                    break;
                case SMS:
                    break;
                default:
                    consoleLogger.log(logMessage, level);
            }
        }
//. . .
        return proceed;
    }

}
