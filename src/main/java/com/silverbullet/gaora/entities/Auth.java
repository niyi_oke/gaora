/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.entities;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * This is a Bean class for holder user authentication data
 * The object of this class will be cached in the CacheManager
 * i.e will be used in populating the CacheManager
 * 
 * @author prodigy4440
 * @param <String> Token used in authenticating the user
 * @param <V> The value or data to be cached about the user
 */
public class Auth<String, V> implements Serializable{

  private static final long serialVersionUID = 5621123846082548926L;
  
  private String token;
  
  private V v;

  public Auth() {
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public V getV() {
    return v;
  }

  public void setV(V v) {
    this.v = v;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 97 * hash + Objects.hashCode(this.token);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Auth<?, ?> other = (Auth<?, ?>) obj;
    if (!Objects.equals(this.token, other.token)) {
      return false;
    }
    return true;
  }
  
}
